//[SECTION] Dependencies and Modules
	const Course = require('../models/Course');

//[SECTION] funcntionality [Create]
	module.exports.addCourse = (info) => {
		let cName = info.course.name;
		let cDesc = info.course.description;
		let cCost = info.course.price;
		let newCourse = new Course({
			name: cName,
			description: cDesc,
			price: cCost
		});
			return newCourse.save().then((savedCourse, err) => {
				if (savedCourse) {
					return savedCourse; 
				} else {
					return false;
				}
			});
		};

//[SECTION] funcntionality [Retrieve]
	//Retrieve all Course (Admin)
	module.exports.getAllCourses = () => {
		return Course.find({}).then(result => {
			return result;
		});
	};

	//Retrieve ONLY Active Course
	module.exports.getAllActive = () => {
		return Course.find({isActive: true}).then(result => {
			return result;
		});
	};

	//Retrieve Single Course
	module.exports.getCourse = (id) => {
		console.log(id);
		return Course.findById(id).then(result => {
			return result;
		});
	};

//[SECTION] funcntionality [Update]
	module.exports.updateCourse = (course, details) => {
		let cName = details.name;	
		let cDesc = details.description;
		let cCost= details.price;
		let updatedCourse ={	
			name: cName,
			description:cDesc,
			price: cCost
		};
		let id = course.courseId;
		return Course.findByIdAndUpdate(id, updatedCourse).then(
			(courseUpdated, err) => {
				if (courseUpdated) {
					return true;
				} else {
					return 'Failed to Update Course';
				}
			});
	};
	//Archive Course
	module.exports.archiveCourse = (course) => {
		let id = course.courseId;
			let updates = {
			isActive: false
			}
			return Course.findByIdAndUpdate(id ,updates).then (
			(archived, err) => {
				if (archived) {
					return 'Course Archived';
				} else {
					return false;
				}
			});
	};

//[SECTION] funcntionality [Delete]
	//Delete Course
	module.exports.deleteCourse = (courseId) => {
		return Course.findByIdAndRemove(courseId).then((removedCourse, err) => {
			if (removedCourse) {
				return 'Course Successfully Removed'
			} else {
				return 'No Course Was Removed'
			};
		});
	};