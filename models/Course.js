//[SECTION] Dependencies and Modules
const mongoose = require('mongoose');

//[Section] Schema
const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'Name is Required']
	},
	description: {
		type: String,
		required: [true, 'Description is Required']
	},
	price: {
		type: Number,
		required: [true, 'Price is Required']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "User's ID is Required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}
	] 
	});

// [Section] Model
module.exports = mongoose.model("Course", courseSchema);
