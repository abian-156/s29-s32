//[SECTION] Dependencies and Modules
	const exp = require('express');
	const controller =require('../controllers/courses');
	const auth = require('../auth');

//[SECTION] Routing Component
	const route = exp.Router();

//[SECTION]-[Post] Route (administrator only)
	route.post('/', auth.verify ,(req, res) => {
		let isAdmin = auth.decode(req.headers.authorization).isAdmin;
		let data = {
			course: req.body,
		}
		if (isAdmin) {
			controller.addCourse(data)
		.then(outcome => {
			res.send(outcome)
		});
		} else {
			res.send('User Unauthorized to Proceed!');
		};
	});
	
//[SECTION]-[Get] Route (administrator only)
	route.get('/all', auth.verify ,(req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let isAdmin	=  payload.isAdmin;
		(isAdmin) ? controller.getAllCourses().then(outcome =>res.send(outcome))   
		: res.send('Unauthorized User');
	});

	//Retrieve Active Courses
	route.get('/', (req, res) => {
		controller.getAllActive().then(outcome => {
			res.send(outcome);
		});
	});

	//Retrieve single Course
	route.get('/:id', (req, res) => {
		let id = req.params.id
		//console.log(id);
		controller.getCourse(id).then(result => {
			res.send(result);
		})
	});

//[SECTION]-[Put] Route (Administrator only)
	route.put('/:courseId', auth.verify ,(req, res) => {
		let params = req.params;
		let body = req.body;		
		if (!auth.decode(req.headers.authorization).isAdmin) {
			res.send('User Unauthorized');
		} else {
			controller.updateCourse(params, body).then(outcome => {
			res.send(outcome);
			});
		};
	});

	//Course Archive (Administrator only)
	route.put('/:courseId/archive', auth.verify ,(req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let params = req.params;
			(isAdmin) ? controller.archiveCourse(params).then(result =>res.send(result))
		 : res.send('Unauthorized User');
	});

//[SECTION]-[Del] Route (administrator only)
	route.delete('/:courseId', auth.verify ,(req,res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin
		let id = req.params.courseId;
		(isAdmin) ? controller.deleteCourse(id).then(outcome =>res.send(outcome))
		: res.send('Unauthorized User');
	});

//[SECTION] Export Route System
 	module.exports = route;